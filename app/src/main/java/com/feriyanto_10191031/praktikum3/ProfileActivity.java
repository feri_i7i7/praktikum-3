package com.feriyanto_10191031.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    TextView txtUsername, txtNim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txtUsername = findViewById(R.id.username);
        txtNim = findViewById(R.id.nim);

        sharedPreferences = getSharedPreferences("biodata", MODE_PRIVATE);

        txtUsername.setText(sharedPreferences.getString("nama", null));
        txtNim.setText(sharedPreferences.getString("nim", null));
    }
}